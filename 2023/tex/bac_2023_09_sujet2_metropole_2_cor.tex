
\medskip

On considère la fonction $f$ définie sur l'intervalle $] 0~;~+\infty[$ par 
$f(x) = (2-\ln x) \times \ln x,$

où $\ln$ désigne la fonction logarithme népérien.

On admet que la fonction $f$ est deux fois dérivable sur $]0~;~+\infty[$.
On note $C$ la courbe représentative de la fonction $f$ dans un repère orthogonal et $C'$ la courbe représentative de la fonction $f'$, fonction dérivée de la fonction $f$.

La \textbf{courbe} $\boldsymbol{C}'$ est donnée ci-dessous ainsi que son unique tangente horizontale $(T)$.

\begin{figure}[t]
\centering
\psset{xunit=0.7cm, yunit=3.5cm,comma}
\def\xmin {-1.9}   \def\xmax {17.8} \def\ymin {-0.39}   \def\ymax {2.18}
\begin{pspicture*}(\xmin,\ymin)(\xmax,\ymax)
\psgrid[xunit=3.5cm,subgriddiv=5, gridlabels=0,griddots=5](0,\ymin)(\xmax,\ymax)
\psaxes[labelFontSize=\small, arrowsize=3pt 2, ticksize=-2pt 2pt,Dy=0.2]{->}(0,0)(-0.9,\ymin)(\xmax,\ymax)
\uput[dl](0,0){O}
\def\f{2 1 x ln sub mul x div}% définition de la fonction
\psplot[plotpoints=2000,linecolor=blue]{0.1}{\xmax}{\f}
\psline[linecolor=red](0,-0.27)(\xmax,-0.27)
\psline[linestyle=dashed,dash=3pt 3pt, linecolor=red](7.39,0)(7.39,-0.27)
\rput*(9.5,1.8){\fbox{\parbox{9cm}{\centering\textbf{On rappelle que cette courbe $\boldsymbol{C'}$ est la courbe représentative de la fonction dérivée $\boldsymbol{f'}$}}}}
\uput[ur](1.27,1.2){\blue $C'$}
\uput[d](1,-0.27){\red $(T)$}
%%%%%%%%%%%%
\psline[ArrowInside=->,linecolor=red,arrowscale=2,linestyle=dashed](1,0)(1,2)(0,2)
\psline[linecolor=red,linewidth=2pt]{]-}(7.39,0)(\xmax,0)
\uput[ur](7.39,0){\small\red $7,4$}
\end{pspicture*}
\end{figure}

\begin{enumerate}
	\item% Par lecture graphique, avec la précision que permet le tracé ci-dessus, donner :

	\begin{enumerate}
		\item Le coefficient directeur de la tangente à $C$ au point d'abscisse 1 est $f'(1)$; graphiquement, c'est environ 2.

		\item 	Le plus grand intervalle sur lequel la fonction $f$ est convexe est le plus grand intervalle sur lequel la fonction $f'$ est croissante, soit  $[7,4\;;\;+\infty[$.
	\end{enumerate}

	\item \begin{enumerate}
		\item %Calculer la limite de la fonction $f$ en $+\infty$.
$\left .\begin{array}{l}
\ds\lim _{x\to +\infty} \ln x = +\infty\\
\ds\lim _{x\to +\infty} 2-\ln x = -\infty
\end{array}		
\right \}
\ds\lim _{x\to +\infty} \left (2-\ln x\right )\times \ln x = -\infty$
et donc
$\ds\lim _{x\to +\infty} f(x) = -\infty$.

		\item% Calculer $\lim\limits_{x \to 0} f(x)$. Interpréter graphiquement ce résultat.
$\left .\begin{array}{l}
\ds\lim _{x\to 0} \ln x = -\infty\\
\ds\lim _{x\to +\infty} 2-\ln x = +\infty
\end{array}		
\right \}
\ds\lim _{x\to 0} \left (2-\ln x\right )\times \ln x = -\infty$
et donc
$\ds\lim _{x\to 0} f(x) = -\infty$.
		
On en déduit que la courbe $C$ admet la droite d'équation $x=0$ comme asymptote verticale.		
	\end{enumerate}

	\item %Montrer que la courbe $C$ coupe l'axe des abscisses en deux points exactement dont on précisera les coordonnées.
Les abscisses des points d'intersection de la courbe $C$ et de l'axe des abscisses sont solutions de l'équation $f(x)=0$; on résout cette équation.

$f(x)=0
\iff
(2-\ln x)\times \ln x=0
\iff
2-\ln x = 0 \text{ ou }	\ln x = 0
\iff
\ln x = 2 \text{ ou } \ln x = 0\\
\phantom{f(x)=0}
\iff
x=\e^{2} \text{ ou } x=1$

Les coordonnées des  points d'intersection de la courbe $C$ et de l'axe des abscisses sont donc \\$(1\;;\;0)$ et $(\e^{2}\;;\;0)$.

	\item \begin{enumerate}
		\item Pour tout réel $x$ appartenant à $]0~;~+\infty[$: %\quad f'(x) = \dfrac{2(1-\ln x)}{x}$.
		
$f'(x)= \left (0-\dfrac{1}{x}\right ) \times \ln x + (2-\ln x)\times \dfrac{1}{x}
= -\dfrac{\ln x}{x} + \dfrac{2-\ln x}{x}
= \dfrac{2-2\ln x}{x}
= \dfrac{2 \left (1-\ln x\right )}{x}$

		\item %En déduire, en justifiant, le tableau de variations de la fonction $f$ sur $]0~;~+\infty[$.
$f'(x)$ s'annule et change de signe quand $1-\ln x = 0$, donc pour $x=\e$.

$f(\e)= (2-\ln\e)\times \ln\e = (2-1)\times 1=1$

D'où le tableau de variations de la fonction $f$:

\begin{center}
{\renewcommand{\arraystretch}{1.3}
\psset{nodesep=3pt,arrowsize=2pt 3}%  paramètres
\def\esp{\hspace*{2.5cm}}% pour modifier la largeur du tableau
\def\hauteur{0pt}% mettre au moins 20pt pour augmenter la hauteur
$\begin{array}{|c| l *4{c}|}
\hline
x & 0  & \esp & \e & \esp & +\infty \\ 
\hline
x & 0  &   \pmb{+} & \vline\hspace{-2.7pt}{\phantom 0} & \pmb{+} & \\ 
\hline
1-\ln x &\vline\;\vline  &   \pmb{+} & \vline\hspace{-2.7pt}0 & \pmb{-} & \\ 
\hline
f'(x) &\vline\;\vline  &   \pmb{+} & \vline\hspace{-2.7pt}0 & \pmb{-} & \\ 
\hline
 &\vline\;\vline &  &   \Rnode{max}{1}  &  &   \\  
f(x) &\vline\;\vline &     &  &  &  \rule{0pt}{\hauteur} \\ 
 &\vline\;\vline \Rnode{min1}{~-\infty} &   &  &  &   \Rnode{min2}{-\infty} \rule{0pt}{\hauteur}    
 \ncline{->}{min1}{max} 
 \ncline{->}{max}{min2} 
 \\ 
\hline
\end{array} $
}
\end{center}			

	\end{enumerate}

	\item On note $f''$ la dérivée seconde de $f$ et on admet que sur $]0~;~+\infty[$, $f''(x) = \dfrac{2(\ln x-2)}{x^{2}}$.

%Déterminer par le calcul le plus grand intervalle sur lequel la fonction $f$ est convexe et préciser les coordonnées du point d'inflexion de la courbe $C$.

La fonction $f$ est convexe si et seulement si $f''(x)\geqslant 0$; on résout cette inéquation.

$f''(x)\geqslant0
\iff
\dfrac{2(\ln x-2)}{x^{2}} \geqslant 0
\iff
\ln x - 2\geqslant 0
\iff
\ln x \geqslant 2
\iff
x\geqslant \e^{2}$

Le plus grand intervalle sur lequel la fonction $f$ est convexe est donc
$\left [  \e^{2}\;;\;+\infty\right [$.

$f''(x)=0 \iff 2-\ln x=0 \iff x=\e^{2}$ et
$f(\e^{2})=(2-\ln \e^{2})\times \ln \e^{2}=0$

Donc la courbe $C$ admet le point de coordonnées $(\e^{2}\;;\;0)$ comme point d'inflexion.

\end{enumerate}

\vspace{1cm}
