
\medskip

\emph{Pour chacune des cinq questions de cet exercice, une seule des quatre réponses proposées est exacte.\\
 Le candidat indiquera sur sa copie le numéro de la question et la réponse choisie.\\
Aucune justification n'est demandée.\\
Une réponse fausse, une réponse multiple ou l'absence de réponse à une question ne rapporte ni n'enlève de point.}

\bigskip

Une urne contient 15 billes indiscernables au toucher, numérotées de 1 à 15. 

La bille numérotée 1 est rouge.

Les billes numérotées 2 à 5 sont bleues.

Les autres billes sont vertes.

\medskip

On choisit une bille au hasard dans l'urne.

On note $R$ (respectivement $B$ et $V$) l'évènement : \og La bille tirée est rouge \fg{} (respectivement bleue et verte).

\medskip

\textbf{Question 1 :}

\medskip

Quelle est la probabilité que la bille tirée soit bleue ou numérotée d'un nombre pair ?

\begin{center}
\begin{tabularx}{\linewidth}{|*{4}{>{\centering \arraybackslash}X|}}\hline
Réponse A&Réponse B&Réponse C&Réponse D\\
$\dfrac{7}{15}$&$\dfrac{9}{15}$&$\dfrac{11}{10}$&\small Aucune des
affirmations précédentes n'est juste.\\ \hline
\end{tabularx}
\end{center}

\medskip

\textbf{Question 2 :}

\medskip

Sachant que la bille tirée est verte, quelle est la probabilité qu'elle soit numérotée 7?

\begin{center}
\begin{tabularx}{\linewidth}{|*{4}{>{\centering \arraybackslash}X|}}\hline
Réponse A&Réponse B&Réponse C&Réponse D\\
$\dfrac{1}{15}$&$\dfrac{7}{15}$&$\dfrac{1}{10}$&\small Aucune des
affirmations précédentes n'est juste.\\ \hline
\end{tabularx}
\end{center}

Un jeu est mis en place. Pour pouvoir jouer, le joueur paie la somme de $10$ euros appelée la mise.

Ce jeu consiste à tirer une bille au hasard dans l'urne.
\begin{itemize}
\item[$\bullet~~$]Si la bille tirée est bleue, le joueur remporte, en euro, trois fois le numéro de la bille. 
\item[$\bullet~~$]Si la bille tirée est verte, le joueur remporte, en euro, le numéro de la bille.
\item[$\bullet~~$]Si la bille tirée est rouge, le joueur ne remporte rien.
\end{itemize}
\smallskip

On note $G$ la variable aléatoire qui donne le gain algébrique du joueur, c'est-à-dire la différence entre ce qu'il remporte et sa mise de départ. 

Par exemple, si le joueur tire la bille bleue numérotée 3, alors son gain algébrique est $-1$ euro.

\medskip

\textbf{Question 3 :}

\medskip

Que vaut P(G = 5)?

\begin{center}
\begin{tabularx}{\linewidth}{|*{4}{>{\centering \arraybackslash}X|}}\hline
Réponse A&Réponse B&Réponse C&Réponse D\\
$\dfrac{1}{15}$&$\dfrac{2}{15}$&$\dfrac{1}{3}$&\small Aucune des
affirmations précédentes n'est juste.\\ \hline
\end{tabularx}
\end{center}

\medskip

\textbf{Question 4 :}

\medskip

Quelle est la valeur de $P_R(G = 0)$ ?

\begin{center}
\begin{tabularx}{\linewidth}{|*{4}{>{\centering \arraybackslash}X|}}\hline
Réponse A&Réponse B&Réponse C&Réponse D\\
$0$&$\dfrac{1}{15}$&$1$&\small Aucune des
affirmations précédentes n'est juste.\\ \hline
\end{tabularx}
\end{center}

\medskip

\textbf{Question 5 :}

\medskip

Que vaut $P_{(G = - 4)}(V)$ ? 

\begin{center}
\begin{tabularx}{\linewidth}{|*{4}{>{\centering \arraybackslash}X|}}\hline
Réponse A&Réponse B&Réponse C&Réponse D\\
$\dfrac{1}{15}$&$\dfrac{4}{15}$&$\dfrac{1}{2}$&\small Aucune des
affirmations précédentes n'est juste.\\ \hline
\end{tabularx}
\end{center}


