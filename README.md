# Épreuve de mathématique du BAC

L'APMEP réalise depuis très longtemps la transcription des annales au format LaTeX. Les contributeurs de Coopmaths.fr ont souhaité utilisé ce travail avec un découpage et un classement exercice par exercice.

Sébastien Lozano a réalisé un script qui analyse les fichiers LaTeX des sujets pour les découper et créer une image utilisée dans le navigateur pour les aperçus.

La communauté a ensuite ajouté les mots clés pour intégrer ces exercices à coopmaths.fr/alea.
