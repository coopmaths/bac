\documentclass[11pt,a4paper,french]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{makeidx}
\usepackage{amsmath,amssymb}
\usepackage{fancybox}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{lscape}
\usepackage{multicol}
\usepackage{mathrsfs}
\usepackage{tabularx}
\usepackage{multirow}
\usepackage{enumitem}
\usepackage{textcomp}
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
%Corrigé : Valérie Tamboise et François Kriegk
%Relecture : Denis Vergès
\usepackage{pst-plot,pst-tree,pstricks,pst-node,pst-text}
\usepackage{pst-eucl,pst-3dplot,pstricks-add}
\usepackage{tikz,tkz-tab}
\usepackage{esvect}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=2cm, bottom=3cm]{geometry}
\headheight15 mm
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\newcommand{\e}{\text{e}}
\usepackage{fancyhdr}
\usepackage[dvips]{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {Baccalauréat Spécialité},
pdftitle = {Centres étrangers (Europe) Sujet 2 6 juin 2024},
allbordercolors = white,
pdfstartview=FitH}
\usepackage{babel}
\usepackage[np]{numprint}
\renewcommand\arraystretch{1.3}
\frenchsetup{StandardLists=true}
\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Baccalauréat spécialité sujet 2}
\lfoot{\small{Centres étrangers}}
\rfoot{\small{6 juin 2024}}
\pagestyle{fancy}
\thispagestyle{empty}

\begin{center}{\Large\textbf{\decofourleft~Baccalauréat Centres étrangers\footnote{Europe} 6 juin 2024~\decofourright\\[7pt]  Sujet 2\\[7pt] ÉPREUVE D'ENSEIGNEMENT DE SPÉCIALITÉ}}
\end{center}

\medskip

\textbf{\textsc{Exercice 1} \hfill 5 points}

\medskip
\begin{enumerate}
\item Un tirage est donc un triplet (c'est-à-dire une 3-liste ou un 3-uplet) ordonné d'éléments choisis dans un ensemble $E$ de cardinal $\mathrm{Card}(E) =8$ (il y a 8 jetons), qui ne s'amenuise pas (car on remet le jeton avant de tirer le suivant, et donc les répétitions sont possibles.)

Le nombre de tirages possibles est donc de : \quad $\mathrm{Card}(E)^3 = 8^3 = 512$.

Il y a 512 tirages possibles.

\item \begin{enumerate}
	\item Si on souhaite un tirage sans répétition de numéro, alors, c'est comme si on avait fait un tirage sans remise (un numéro déjà choisi ne peut pas être choisi à nouveau), et donc un triplet ordonné d'éléments choisis dans un ensemble qui s'amenuise.

	Il y a donc $\dfrac{8!}{(8-3)!} = 8 \times 7 \times 6 = 336$ tirages possibles sans numéro répété.

	\emph{Remarque :} On peut parler d'arrangement, même si ce mot n'est pas explicitement à connaître en terminale.

	On peut aussi voir cette valeur comme étant : 8 choix possibles pour le premier numéro du triplet, puis 7 choix possibles pour le deuxième numéro, sachant qu'il doit être différent du premier et enfin 6 choix possibles pour le dernier numéro, qui doit être différent des deux précédents.

	\item Le nombre de tirages avec au moins une répétition de numéro est donc la différence des deux résultats précédents : $512 - 336 = 176$.

	En effet, l'ensemble des tirages possibles est la réunion de deux ensembles disjoints : les tirages sans aucune répétition de numéro et les tirages avec au moins une répétition de numéro.

	\emph{Remarque :} On pouvait aussi compter le nombre de tirages avec deux numéros identiques : $8 \times \displaystyle \binom{3}{2} \times 7= 168$ (huit choix pour le numéro répété, multiplié par $\displaystyle \binom{3}{2}$ façons de placer ces deux numéros identiques dans le triplet, multiplié par sept choix pour le numéro différent des deux premiers) et l'ajouter au nombre de tirages avec trois numéros identiques : $8$ (huit choix possibles pour le numéro qui sera répété trois fois), mais dans ce cas, on obtient 176 sans le déduire des questions précédentes.
\end{enumerate}

\item Le sac étant opaque et les jetons indiscernables au toucher, chaque sélection d'un jeton dans le sac est une situation d'équiprobabilité, et donc on a une loi équirépartie.

On peut donc présenter la loi de probabilité de la variable aléatoire $X_1$ sous la forme du tableau suivant :

\renewcommand\arraystretch{2}
\begin{tabularx}{\linewidth}{|l|*{8}{>{\centering \arraybackslash}X|}} \hline
	$x_i$ & 1&2&3&4&5&6&7&8\\ \hline
	$P(X_1 = x_i)$&$ \dfrac{1}{8} $&$ \dfrac{1}{8} $&$ \dfrac{1}{8} $&$ \dfrac{1}{8} $&$ \dfrac{1}{8} $&$ \dfrac{1}{8} $&$ \dfrac{1}{8} $&$ \dfrac{1}{8} $\\ \hline
\end{tabularx}
\renewcommand\arraystretch{1}
\item L'espérance de la variable aléatoire $X_1$ est donc donnée par :

$\aligned[t] E(X_1) &= x_1 \times P(X_1 = x_1) + x_2 \times P(X_1 = x_2) + ... x_8 \times P(X_1 = x_8) \\
&= \big(x_1 + x_2 + ... + x_8\big) \times\dfrac{1}{8}\quad \text{car la loi est équirépartie}\\
&= \big(1 + 2 + ... + 8\big) \times\dfrac{1}{8}\\
&= \dfrac{1 + 8}{2}\times 8 \times \dfrac{1}{8}\quad \text{avec la formule de la somme des premiers termes d'une}\\
&\hphantom{\dfrac{1 + 8}{2}\times 8 \times \dfrac{1}{8}}\qquad\text{ suite arithmétique}\\
&= 4,5\endaligned$

\item On a $E(S) = E(X_1 + X_2 + X_3)= E(X_1) + E(X_2) + E(X_3)$, et puisque $X_1$, $X_2$ et $X_3$ suivent la même loi de probabilité, alors on en déduit :

$E(S) = 3\times E(X_1) = 3\times 4,5 = 13,5$

\item L'évènement $\{S = 24\}$ n'est réalisé que par le tirage $(8~;~8~;~8)$, donc sur les 512 issues possibles de l'expérience aléatoire, une seule est favorable à l'évènement : la probabilité est donc de $\dfrac{1}{512}$ (puisque l'on est en situation d'équiprobabilité).

\emph{Remarque :} On peut aussi dire que l'évènement $\{S = 24\}$ est égal à l'évènement $\{X_1 = 8\} \cap\{X_2 = 8\} \cap\{X_3 = 8\}$.

Et comme les variables aléatoires $X_1$, $X_2$ et $X_3$ sont indépendantes :

$\aligned[t] P(S = 24) &= P \big(\{X_1 = 8\} \cap\{X_2 = 8\} \cap\{X_3 = 8\}\big) = P(X_1 = 8) \times P(X_2 = 8) \times P(X_3 = 8)\\
&= \left(\dfrac{1}{8}\right)^3 = \dfrac{1}{512}\endaligned$
\item \begin{enumerate}
		\item \begin{itemize}
			\item Le nombre 24 ne peut être obtenu que par la somme 8 + 8 + 8, qui ne procède que d'un seul tirage (le tirage $(8~;~8~;~8)$).

			\item Le nombre 23 ne peut être obtenu que par la somme 7 + 8 + 8, cette somme est constituée d'un doublon (le 8) et d'un entier présent une seule fois (le 7).

			Elle peut procéder de trois tirages différents, car il y a $ \displaystyle \binom{3}{2} = 3$ façons de placer les deux entiers identiques dans un triplet.

			Les trois triplets conduisant à $\{S = 23\}$ sont : $ (7~;~8~;~8) $~;~ $ (8~;~7;8) $ et $ (8~;~8~;~7)$.

			\item Le nombre 22 peut être obtenu comme somme de trois entiers naturels entre 1 et 8 en faisant : 6 + 8 + 8, ou bien en faisant 7 + 7 + 8.

			Chacune de ces sommes est constitué d'un doublon  et d'un troisième entier différent, et est donc le résultat de trois tirages différents.

			Il y a donc six tirages donnant 22 (les tirages $(6~;~8~;~8)$; $ (8~;~6~;~8)$ ; $(8~;~8~;~6)$ ; $ (8~;~7~;~7)$ ; $ (7~;~8;7)$ et $ (7~;~7~;~8)$).
		\end{itemize}

On a donc bien finalement $1 + 3 + 6 = 10$ tirages conduisant au gain d'un lot.


		\item On est en situation d'équiprobabilité pour les 512 tirages possibles (nombre déterminé à la question \textbf{1.}), donc, avec 10 tirages favorables à l'évènement \og gagner un lot\fg{}, la probabilité de gagner un lot est donc de $ \dfrac{10}{512} = \dfrac{5}{256} $.
	\end{enumerate}
\end{enumerate}

\medskip

\textbf{\textsc{Exercice 2} \hfill 6 points}

\medskip

\begin{enumerate}
\item \begin{enumerate}
		\item La fonction exponentielle est continue sur $\R$ donc :\quad $\lim\limits_{x \to 1} \e^{x} = \e^1 = \e >0$.

		Par limite de la somme, on a :\quad $\lim\limits_{x \to 1} x - 1 = 0$, et comme on travaille sur $]-\infty~;~ 1[ $, on a $x - 1 < 0$. (on peut noter $\lim\limits_{x \to 1} x - 1 = 0^-$ ).

		Par limite du quotient, on a :\quad $\lim\limits_{x \to 1} f(x) = -\infty$.

		\item On en déduit que la courbe $ \mathcal{C} $ admet une asymptote verticale, d'équation $x = 1$.

	\end{enumerate}
	\item On a : \quad $\lim\limits_{x \to -\infty} \e^x = 0$;

	Par limite de la somme, on a :\quad $\lim\limits_{x \to -\infty} x - 1 =-\infty$,

	Par limite du quotient, on en déduit :\quad $\lim\limits_{x \to -\infty} f(x) = 0$.

	On en déduit que $ \mathcal{C} $ admet également une asymptote, d'équation $y = 0$, au voisinage de $ -\infty $.

\item \begin{enumerate}
		\item $f$ est dérivable sur $ ]-\infty ~;~ 1[ $, en tant que quotient de fonctions définies et dérivables sur cet intervalle, avec la fonction au dénominatuer ne s'annulant pas sur l'intervalle.

		$\aligned[t] \forall x \in ]-\infty~;~1[ ,\quad f'(x) & = \dfrac{\e^x \times (x - 1) - \e^x \times 1}{(x-1)^2 }\\
		&= \dfrac{\e^x \times (x - 1 - 1)}{(x-1)^2}\\
		&= \dfrac{(x - 2)\e^x}{(x - 1)^2}\\ \endaligned$

		\item La fonction exponentielle est à valeurs strictement positives sur $\R$, et pour tout $x$ dans $ ]-\infty ~;~ 1[ $, $(x - 1)^2$ est strictement positif, donc le signe de $f'(x)$ est le même que le signe de $(x - 2)$.

$x - 2 \geqslant 0 \iff x \geqslant 2$, donc sur $ ]-\infty ~;~ 1[ $, $(x-2)$ est  strictement négatif, donc $f'(x)$ également.

Finalement, on peut donc en déduire que $f$ est strictement décroissante sur $]-\infty ~;~ 1[$, et donc, on a le tableau de variations suivant (avec les limites justifiées aux questions \textbf{1. a.} et \textbf{2.}):

\begin{center}
			\begin{tikzpicture}
			\tkzTabInit[lgt=3, espcl=4]{$x$/0.8,signe de $f'(x)$/0.8,variations de $f$/1.5}{$ -\infty $ , 1}
			\tkzTabLine{t,-,d}
			\tkzTabVar{+/0,-D/$-\infty$}
			\end{tikzpicture}
\end{center}
	\end{enumerate}

\item \begin{enumerate}
		\item Pour étudier la convexité de la fonction $f$ sur l'intervalle $]-\infty~;~1[$, on va étudier le signe de $f''(x)$.

		Comme, pour tout $x$ dans $ ]-\infty ~;~1[ $, on a $(x - 1) < 0$ et donc $(x - 1)^3 < 0$ et $\e^{x} > 0$, on en déduit que le signe de $f''(x)$ est l'opposé du signe du trinôme : $x^2 - 4x + 5$.

Or, ce trinôme a un discriminant $\Delta = (-4)^2 - 4 \times 1 \times 5 = -4 $ qui est strictement négatif, donc n'admet pas de racine, et donne des images strictement positives (car le coefficient dominant est positif) pour tout réel $x$.

\emph{Rem.} On peut écrire :

$x^2 - 4x + 5 = (x - 2)^2 - 4 + 5 = (x - 2)^2 + 1 \geqslant 1 > 0$ : le trinôme est positif quel que soit $x \in \R$.

Finalement, la dérivée seconde $f''$ est à valeurs strictement négatives sur $\left]-\infty ~;~ 1\right[$, on en déduit que la fonction $f$ est concave sur $]-\infty ~;~ 1[$.

		\item Pour déterminer l'équation de $T$, il nous faut connaître $f'(0)$ et $f(0)$ :

\begin{itemize}[left=5mm]
\item $f'(0) = \dfrac{(0 - 2)\e^{0}}{(0 - 1)^2} = \dfrac{-2}{1} = -2$;
\item $f(0) = \dfrac{\e^0}{0 - 1} = \dfrac{1}{-1} = -1$.
\end{itemize}

La formule classique donne une équation pour $T$ :

$y = f'(0)(x - 0) + f(0) \iff y = - 2x - 1$

L'équation réduite de $T$ est donc : $y = - 2x - 1$.

		\item Puisque $f$ est concave sur l'intervalle $]-\infty~;~1[$, la courbe $\mathcal{C}$ est donc située sous ses tangentes, notamment sous la tangente $T$, sur cet intervalle.

Pour tout réel $x$ dans cet intervalle, l'ordonnée d'un point sur la courbe $\mathcal{C}$ (c'est-à-dire $f(x)$) est donc inférieure ou égale à l'ordonnée du point ayant la même abscisse sur la tangente $T$ (or, sur la tangente $T$, l'ordonnée du point d'abscisse $x$ est $- 2x -1$, d'après la question précédente).

On en déduit donc : \quad $\aligned[t] x \in ]-\infty~;~1[ &\implies f(x) \leqslant -2x -1\\
		&\implies \dfrac{\e^x}{x - 1}\leqslant - 2x - 1\\
		&\implies \e^x \geqslant (x -1)(-2x-1)\\
		& \qquad \quad \text{car sur }]-\infty ~;~ 1[,\quad x - 1 <0\\
		&\implies \e^x \geqslant (-2x - 1)(x - 1)
		\endaligned$

On arrive donc à l'inégalité demandée.
	\end{enumerate}
\item
	\begin{enumerate}
		\item La fonction $f$ est :
		\begin{itemize}[left = 5mm]
			\item continue sur $]-\infty~;~1[$ (car dérivable sur cet intervalle);
			\item strictement décroissante sur $]-\infty~;~1[$ (d'après la question \textbf{3. b.});
			\item telle que $-2$ est une valeur intermédiaire entre $\lim\limits_{-\infty} f = 0$ et $\lim\limits_{1} f = -\infty$;
		\end{itemize}
		D'après le corollaire du théorème des valeurs intermédiaires appliqué aux fonctions strictement monotones, l'équation $f(x) = -2$ admet une unique solution $\alpha$ sur l'intervalle $]-\infty~;~1[$.

		\item Comme on a repéré à la question \textbf{4. b.} que $f(0) = -1$, on sait que la solution sera à chercher dans l'intervalle $]0 ~;~ 1[$.

À l'aide de la calculatrice, par balayage, on a :
\begin{itemize}[left=5mm]
\item $f(0,31) \approx -1,98 > -2$;
\item $f(0,32) \approx-2,03 < -2$;
\end{itemize}

Un encadrement de $\alpha$ d'amplitude $10^{-2}$ est $0,31 < \alpha < 0,32[$.
	\end{enumerate}
\end{enumerate}

\medskip

\textbf{\textsc{Exercice 3} \hfill 5 points}

\medskip


\begin{enumerate}
	\item

	On a : I$\left(\dfrac{1}{2}~;~ 0~;~0 \right)$  et J$\left(1~;~1~;~\dfrac{1}{2} \right)$ .

	\item

	On en déduit :

	$\vect{\text{EJ}} \begin{pmatrix}1-0  \\ 1- 0 \\ \dfrac{1}{2}-1     \end{pmatrix} =  \begin{pmatrix}1 \\ 1 \\ -\dfrac{1}{2}\end{pmatrix}$;
	\qquad
	$\vect{\text{FH}} \begin{pmatrix}0-1   \\ 1-0 \\ 1- 1   \end{pmatrix} =  \begin{pmatrix}-1 \\ 1 \\ 0\end{pmatrix}$;
	\quad	et \quad $\vect{\text{FI}} \begin{pmatrix}\dfrac{1}{2}-1  \\ 0-0  \\ 0-1   \end{pmatrix} =  \begin{pmatrix}-\dfrac{1}{2} \\ 0 \\ -1    \end{pmatrix}$.


	donc :

	$\vect{\text{EJ}} \cdot \vect{\text{FH}}= 1 \times (-1) + 1\times 1 -\dfrac{1}{2} \times 0 = 0$

	$\vect{\text{EJ}} \cdot \vect{\text{FI}}=  -\dfrac{1}{2} \times 1 + 1\times 0 -\dfrac{1}{2} \times (-1) = 0$

	Le vecteur $\vect{\text{EJ}}$ est donc orthogonal à deux vecteurs non colinéaires du plan (FHI) donc $\vect{\text{EJ}}$ est normal au plan (FHI).

	\item Le vecteur $\vect{\text{EJ}}\begin{pmatrix}1 \\ 1 \\ -\dfrac{1}{2}    \end{pmatrix}$ est normal au plan (FHI) donc le vecteur   $\vect{n}= -2\vect{\text{EJ}}\begin{pmatrix}-2 \\ -2 \\ 1    \end{pmatrix}$ est aussi un vecteur normal au plan (FHI).

Une équation cartésienne du plan (FHI)  est donc de la forme : \quad   $-2x-2y+z+d=0$ avec $d \in \R$.

De plus, le point F appartient au plan (FHI) donc ses coordonnées verifient l'équation du plan. On a donc :

$\aligned[t] \mathrm{F} \in (\mathrm{FHI}) &\iff -2\times x_\mathrm{F}  -2 \times y_\mathrm{F} + z_\mathrm{F} +d=0 \\
	&\iff -2\times 1  -2 \times 0 + 1 +d=0 \\
	&\iff -2+1+d=0 \\
	&\iff d=1\endaligned$

Une équation cartésienne du plan (FHI) est donc	$-2x-2y+ z+1=0$.
 \item Le vecteur $\vect{\text{EJ}} $ est un vecteur directeur de la droite (EJ) et E est un point de la droite (EJ).  Une représentation paramétrique de la droite (EJ) est donc :

$\begin{cases} x= 0 + 1\times t \\ y=0 + 1 \times t  \\ z=1 - \dfrac{1}{2} \times t  \end{cases} $ avec $t \in \R$ \quad c'est à dire \quad : $\begin{cases} x= t \\ y=t  \\ z=1 - \dfrac{1}{2} t  \end{cases} $ avec $t \in \R$

	\item
	\begin{enumerate}
		\item Soit K est le projeté orthogonal du point E sur le plan (FHI).

K est donc l'intersection du plan (FHI) et de la droite orthogonale au plan (FHI) passant par E, c'est à dire la droite (EJ).

Pour cela, on va considérer le point $M_t$ de paramètre $t$ sur la droite $(\mathrm{EJ})$.

$\aligned M_t \in (\mathrm{FHI}) &\iff -2x_{M_t} -2y_{M_t}+z_{M_t} +1 = 0\\
&\iff -2\times t -2  \times t  +  \left( 1- \dfrac{1}{2}t\right) +1=0 \\
&\iff -2t-2t+1-\dfrac{1}{2}t+1=0 \\
&\iff -\dfrac{9}{2}t = -2 \\
&\iff t= \dfrac{4}{9} \endaligned$

Le seul point de la droite étant aussi sur le plan est donc K, c'est le point de paramètre $\dfrac{4}{9}$ dans l'équation paramétrique de $(\mathrm{EJ})$.

		Ses coordonnées vérifient :

		$x_\mathrm{K}=\dfrac{4}{9}$, \quad $y_\mathrm{K}=\dfrac{4}{9}$ \quad et \quad $z_\mathrm{K}=1- \dfrac{1}{2} \times \dfrac{4}{9}=1-\dfrac{2}{9}=\dfrac{7}{9}$.

		Le projeté orthogonal  du point E sur le plan (FHI) est le point K de coordonnées K$\left( \dfrac{4}{9} ~;~  \dfrac{4}{9} ~;~ \dfrac{7}{9} \right)$.


		\item  Le volume $V$ d'une pyramide est donné par  $V=\dfrac{1}{3}\mathcal{B}h$  où  $\mathcal{B}$ est l'aire de la base et $h$ la hauteur correspondante.

		Prenons le triangle EFH comme base, la hauteur issue de  I est donc la droite (IL) et la distance de I au plan EFH est donc égale à la longueur IL.

		EFH est un triangle rectangle en E et son aire est égale à $\dfrac{ 1\times 1}{2}=\dfrac{1}{2}$

		L a pour coordonnées  L$\left(\dfrac{1}{2}~;~ 0~;~1 \right)$   donc

		LI$^2= \left( \dfrac{1}{2} -\dfrac{1}{2}   \right)^2 + (0-0)^2 + (1-0)^2= 1 $ donc LI$=1$.

		On a donc V= $\dfrac{1}{3} \times \dfrac{1}{2} \times 1 = \dfrac{1}{6}~\left(\text{cm}^3\right)$.


		\item  Calculons maintenant le volume en prenant le triangle FHI comme base. La hauteur sera donc la longueur EK.

		EK$^2=    \left( \dfrac{4}{9} -0   \right)^2 + \left( \dfrac{4}{9} -0  \right)^2 + \left( \dfrac{7}{9} -1  \right)^2  = \dfrac{16}{81}+ \dfrac{16}{81}+\dfrac{4}{81}  = \dfrac{36}{81}$

		donc EK$=\dfrac{6}{9}=\dfrac{2}{3}$

		On a donc :

		$\aligned[t] \dfrac{1}{6}=\dfrac{1}{3}\times \mathcal{A}_{\mathrm{FHI}}\times \dfrac{2}{3}
		&\iff \dfrac{1}{6} \times 3 \times\dfrac{3}{2} =  \mathcal{A}_{\mathrm{FHI}} \\
		&\iff \dfrac{1}{6} \times 3 \times\dfrac{3}{2} =  \mathcal{A}_{\mathrm{FHI}} \\
		&\iff  \mathcal{A}_{\mathrm{FHI}}=\dfrac{3}{4}\endaligned$


		L'aire du triangle FHI est	$ \dfrac{3}{4}~\left(\text{cm}^2\right)$.
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{\textsc{Exercice 4} \hfill 4 points}

\medskip
\textbf{Partie A}

\begin{enumerate}
	\item  $f$ est de la forme $\sqrt{u}$ avec, pour tout $x$ réel positif :\quad  $u(x)= x+1$ \quad et \quad $u'(x)=1$.

	Donc \quad $f'= \dfrac{u'}{2\sqrt{u}}$ \quad donc pour tout $x \in [0~;~+\infty[$,\quad  $f'(x) =   \dfrac{1}{2\sqrt{x+1}}$.

	La fonction racine carrée étant à valeurs positives, pour tout $x \in [0~;~+\infty[$, $f'(x) \geqslant 0$  donc la fonction $f$ est croissante  sur l'intervalle $[0~;~~+\infty[$.

	\item Soit $x$ un réel appartenant à l'intervalle $[0~;~+\infty[$ :

	$f(x) - x =  \sqrt{x+1} -x =    \dfrac {   \left(  \sqrt{x+1} -x    \right) \times   \left(  \sqrt{x+1} +x    \right)  }    {  \sqrt{x+1} +x    }    =      \dfrac {   \left(  \sqrt{x+1}    \right) ^2- x^2  } {   \sqrt{x+1} +x     }     =      \dfrac{-x^2 + x + 1}{\sqrt{x + 1} + x}$.


	\item  $\aligned[t] f(x) = x &\iff  \dfrac{-x^2 + x + 1}{\sqrt{x + 1} + x} = 0  \\
	&\iff -x^2+x+1=0\endaligned$

	Car sur $[0~;~+\infty[$, le dénominateur $\sqrt{x+1} + x$ est strictement positif, en tant que somme d'une expression strictement positive ($\sqrt{x+1}$) et d'une autre positive ($x$), donc ce dénominateur est non nul.

	On a un polynome du second degré. Calculons le discriminant.

	$\Delta = 1^2-4 \times (-1) \times 1= 5$

	On a donc deux racines réelles distinctes :

	$x_1 = \dfrac{-1-\sqrt{5}}{-2}= \dfrac{1 +\sqrt 5}{2}\geqslant 0$


	$x_2= \dfrac{-1+\sqrt{5}}{-2}<0$ \quad donc n'est pas solution de l'équation sur $[0~;~+\infty[$


	L'équation $f(x)=x$ admet donc une unique solution  sur l'intervalle $[0~;~+\infty[$ :

	$\ell = \dfrac{1 +\sqrt 5}{2}$.

	\emph{Remarque :} ce nombre est connu sous l'appellation \og nombre d'or\fg{}.

\end{enumerate}

\medskip
\textbf{Partie B}

\medskip

\begin{enumerate}
	\item \begin{itemize}
		\item \emph{Initialisation :} pour  $n= 0$, on a $u_0 = 5$.

$u_0 \in [0~;~+\infty[$, donc $f(u_0)$ est définie et $u_1 = f(u_0) = f(5) = \sqrt{5+1} = \sqrt{6} \approx 2,45$.

L'inéquation $ \quad 1 \leqslant u_{0+1} \leqslant u_0$ \quad est bien vérifiée.

		\item \emph{Hérédité :} Pour un naturel $n$, on suppose que l'inégalité est vraie au rang $n$, c'est-à-dire : \quad $1 \leqslant u_{n+1} \leqslant u_n$.

En appliquant la fonction $f$ aux trois membres de cette inégalité, la croissance de $f$ sur $[0~;~+\infty[$ donne :

$\aligned f(1 ) \leqslant f(u_{n+1})\leqslant f(u_n) &\implies \sqrt{2} \leqslant u_{n+2} \leqslant u_{n+1}\\
&\implies 1 \leqslant u_{n+2} \leqslant u_{n+1},\quad \emph{car } \sqrt{2} \approx 1,4 \geqslant 1\endaligned$

Cette conclusion est l'inégalité, au rang suivant.

	\item \emph{Conclusion :} l'inégalité est vraie au rang 0, et sa véracité est héréditaire, pour tout entier naturel, donc, en vertu du principe de récurrence, pour tout entier naturel $n$, l'inégalité est vraie, soit : \quad $1 \leqslant u_{n+1} \leqslant u_{n}$.
		\end{itemize}

	\item La question précédente donne :
\begin{itemize}[left=5mm]
		\item $\forall n\in \N, \quad u_{n+1} \leqslant u_n$\quad la suite est donc décroissante;
		\item $\forall n\in \N, \quad 1 \leqslant u_n$\quad la suite est donc minorée, par 1;
	\end{itemize}

La suite $\left(u_n\right)$ étant décroissante et minorée, elle est donc convergente, vers une limite qui doit être supérieure ou égale à 1 (et inférieure ou égale à $u_0$ car la suite est décroissante).

	\item La suite $\left(u_n\right)$ est une suite convergente, définie par récurrence par la relation \linebreak $u_{n+1} = f(u_n)$, où la fonction $f$ est continue (car dérivable) sur $ [0 ~;~ +\infty[$, intervalle qui contient la limite de la suite.

D'après le théorème \og du point fixe \fg{}, on en déduit que la limite ne peut être qu'une solution de l'équation $f(x) = x$ dans l'intervalle $ [0 ~;~ +\infty[ $.

D'après la question \textbf{3.} de la \textbf{Partie A}, cette équation n'a qu'une solution dans \linebreak $ [0 ~;~ +\infty[ $ :\quad $\dfrac{1+\sqrt{5}}{2}$.

On constate également que cette valeur satisfait les critères supplémentaires que l'on connaît pour cette limite (supérieure à 1 et inférieure à $u_0$).

La suite $\left(u_n\right)$ converge donc vers $\ell = \dfrac{1+\sqrt{5}}{2}$.

	\item La fonction \texttt{seuil} présentée initialise la variable \texttt{u} à 5, c'est-à-dire $u_0$ et la variable \texttt{i} à 0, c'est-à-dire l'indice de $u_0$.

Tant que la valeur absolue de la différence entre $\ell$ et u est supérieure ou égale à $10^{-n}$, on remplace dans la variable \texttt{u} le terme de la suite par le terme suivant, et dans la variable \texttt{i} l'indice par l'indice suivant.

		La boucle s'arrête donc dès que \texttt{u} contient un terme de la suite dont la distance à $\ell$ est strictement inférieure à $10^{-n}$ et renvoie l'indice de ce terme (qui est donc une valeur approchée à $10^{-n}$ près de la limite).
		\begin{enumerate}
			\item \texttt{seuil(2)} va donc renvoyer l'indice du premier terme qui est à moins d'un centième de la limite $\ell$.

Par exploration à la calculatrice, on a $u_4 - \ell \approx 0,02 \geqslant 10^{-2}$ et 

$u_5 - \ell \approx 0,007 < 10^{-2}$ donc la fonction renverra l'indice du premier terme pour lequel le test du \texttt{while} n'est pas satisfait : 5.

			\item Si \texttt{seuil(4)} renvoie 9, c'est que le premier terme de la suite qui est une valeur approchée de $\ell$ à $10^{-4}$ près sera $u_9$.

			\emph{Remarque :} comme la suite $\left(u_n\right)$ converge vers $\ell$ en décroissant, tous les ter-mes de la suite sont des valeurs approchées de $\ell$ par excès, l'utilisation de la fonction \texttt{abs}  dans la fonction Python n'était indispensable pour cette fonction de récurrence.
	\end{enumerate}
\end{enumerate}
\end{document}