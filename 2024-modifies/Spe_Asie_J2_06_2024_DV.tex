\documentclass[11pt,a4paper,french]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{makeidx}
\usepackage{amsmath,amssymb}
\usepackage{fancybox}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{lscape}
\usepackage{multicol}
\usepackage{mathrsfs}
\usepackage{tabularx}
\usepackage{multirow}
\usepackage{enumitem}
\usepackage{textcomp}
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
%Relecture : 
\usepackage{pst-plot,pst-tree,pstricks,pst-node,pst-text}
\usepackage{pst-eucl,pst-3dplot,pstricks-add}
\usepackage{tikz}
\usepackage{esvect}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=2.5cm, right=2.5cm, top=2cm, bottom=3cm]{geometry}
\headheight15 mm
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\newcommand{\e}{\text{e}}
\usepackage{fancyhdr}
\usepackage[dvips]{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {Baccalauréat Spécialité},
pdftitle = {Asie Sujet 2 11 juin 2024},
allbordercolors = white,
pdfstartview=FitH}
\usepackage{babel}
\usepackage[np]{numprint}
\renewcommand\arraystretch{1.3}
\frenchsetup{StandardLists=true}
\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Baccalauréat spécialité sujet 2}
\lfoot{\small{Asie}}
\rfoot{\small{11 juin 2024}}
\pagestyle{fancy}
\thispagestyle{empty}

\begin{center}{\Large\textbf{\decofourleft~Baccalauréat Asie 11 juin 2024~\decofourright\\[7pt]  Sujet 2\\[7pt] ÉPREUVE D'ENSEIGNEMENT DE SPÉCIALITÉ}}
\end{center}

\medskip

\textbf{\textsc{Exercice 1} \hfill 5,5 points}

\medskip

On considère la fonction $f$ définie sur $]0 ~;~+ \infty[$ par 

\[f(x) = x^2 - x \ln (x).\]

On admet que $f$ est deux fois dérivable sur $]0 ~;~+ \infty[$.

On note $f'$ la fonction dérivée de la fonction $f$ et $f''$ la fonction dérivée de la fonction $f'$.

\medskip

\textbf{Partie A :} Étude de la fonction $f$

\medskip

\begin{enumerate}
\item Déterminer les limites de la fonction $f$ en $0$ et en $+\infty$.
\item Pour tout réel $x$ strictement positif, calculer $f'(x)$.
\item Montrer que pour tout réel $x$ strictement positif:

\[f''(x) = \dfrac{2x - 1}{x}.\]

\item Étudier les variations de la fonction $f'$ sur $]0 ~;~+ \infty[$, puis dresser le tableau des variations de la fonction $f'$ sur $]0 ~;~+ \infty[$.

On veillera à faire apparaître la valeur exacte de l'extremum de la fonction $f'$ sur $]0 ~;~+ \infty[$.

Les limites de la fonction $f'$ aux bornes de l'intervalle de définition ne sont pas attendues.
\item Montrer que la fonction $f$ est strictement croissante sur $]0 ~;~+ \infty[$.
\end{enumerate}

\medskip

\textbf{Partie B :} Étude d'une fonction auxiliaire pour la résolution de l'équation $f(x) = x $

\medskip

On considère dans cette partie la fonction $g$ définie sur $]0 ~;~+ \infty[$ par 

\[g(x) = x - \ln (x).\]

On admet que la fonction $g$ est dérivable sur $]0 ~;~+ \infty[$, on note $g'$ sa dérivée.

\medskip

\begin{enumerate}
\item Pour tout réel strictement positif, calculer $g'(x)$, puis dresser le tableau des variations de la fonction $g$.

Les limites de la fonction $g$ aux bornes de l'intervalle de définition ne sont pas attendues.
\item On admet que 1 est l'unique solution de l'équation $g(x) = 1$.

Résoudre, sur l'intervalle $]0 ~;~+ \infty[$, l'équation $f(x) = x$.
\end{enumerate}

\medskip

\textbf{Partie C :} Étude d'une suite récurrente

On considère la suite $\left(u_n\right)$ définie par $u_0 = \dfrac12$ et pour tout entier naturel $n$,

\[u_{n+1} = f\left(u_n\right) = u_n^2  - u_n \ln \left(u_n\right).\]

\medskip

\begin{enumerate}
\item Montrer par récurrence que pour tout entier naturel $n$:
\[\dfrac12  \leqslant u_n \leqslant u_{n+1} \leqslant 1.\]

\item Justifier que la suite $\left(u_n\right)$ converge.

On appelle $\ell$ la limite de la suite $\left(u_n\right)$ et on admet que $\ell$ vérifie l'égalité $f(\ell) = \ell$.
\item Déterminer la valeur de $\ell$.
\end{enumerate}

\bigskip

\textbf{\textsc{Exercice 2} \hfill 5,5 points}

\medskip

\emph{Léa passe une bonne partie de ses journées à jouer à un jeu vidéo et s'intéresse aux chances de victoire de ses prochaines parties.}

\medskip

\emph{Elle estime que si elle vient de gagner une partie, elle gagne la suivante dans 70\,\% des cas.}

\medskip

\emph{Mais si elle vient de subir une défaite, d'après elle, la probabilité qu'elle gagne la suivante est de $0,2$.}

\emph{De plus, elle pense avoir autant de chance de gagner la première partie que de la perdre.}

\emph{On s'appuiera sur les affirmations de Léa pour répondre aux questions de cet exercice.}

\medskip

Pour tout entier naturel $n$ non nul, on définit les évènements suivants: 

\begin{itemize}[label=$\bullet~$]
\item $G_n$ : \og Léa gagne la $n$-ième partie de la journée \fg{} ;
\item $D_n$ : \og Léa perd la $n$-ième partie de la journée \fg.
\end{itemize}

\smallskip

Pour tout entier naturel $n$ non nul, on note $g_n$ la probabilité de l'évènement $G_n$.

On a donc $g_1 = 0,5$.

\medskip

\begin{enumerate}
\item Quelle est la valeur de la probabilité conditionnelle $p_{G_1}\left(D_2\right)$ ?
\item Recopier et compléter l'arbre des probabilités ci-dessous qui modélise la situation pour
les deux premières parties de la journée :

\begin{center}
\pstree[treemode=R,levelsep=3.25cm]{\TR{}}
{\pstree{\TR{$G_1~$}\taput{\ldots}}
	{\TR{$G_2~$}\taput{\ldots}
	\TR{$D_2~$}\tbput{\ldots}
	}
\pstree{\TR{$D_1~$}\tbput{\ldots}}
	{\TR{$G_2~$}\taput{\ldots}
	\TR{$D_2~$}\tbput{\ldots}
	}
}
\end{center}

\item Calculer $g_2$.
\item Soit $n$ un entier naturel non nul.
	\begin{enumerate}
		\item Recopier et compléter l'arbre des probabilités ci-dessous qui modélise la situation pour les $n$-ième et $(n + 1)$-ième parties de la journée.

\begin{center}
\pstree[treemode=R,levelsep=3.25cm]{\TR{}}
{\pstree{\TR{$G_n~$}\taput{$g_n$}}
	{\TR{$G_{n+1}~$}\taput{\ldots}
	\TR{$D_{n+1}~$}\tbput{\ldots}
	}
\pstree{\TR{$D_n~$}\tbput{\ldots}}
	{\TR{$G_{n+1}~$}\taput{\ldots}
	\TR{$D_{n+1}~$}\tbput{\ldots}
	}
}
\end{center}
		\item Justifier que pour tout entier naturel $n$ non nul,
		\[g_{n+1} = 0,5g_n + 0,2.\]

	\end{enumerate}
\item Pour tout entier naturel $n$ non nul, on pose $v_n = g_n - 0,4$.
	\begin{enumerate}
		\item Montrer que la suite $\left(v_n\right)$ est géométrique.
		
On précisera son premier terme et sa raison.
		\item Montrer que, pour tout entier naturel $n$ non nul : 
		\[g_n = 0,1 \times  0,5^{n-1} + 0,4.\]
		
	\end{enumerate}
\item Étudier les variations de la suite $\left(g_n\right)$.
\item Donner, en justifiant, la limite de la suite $\left(g_n\right)$.

Interpréter le résultat dans le contexte de l'énoncé.
\item Déterminer, par le calcul, le plus petit entier $n$ tel que $g_n - 0,4 \leqslant 0,001$.
\item Recopier et compléter les lignes 4, 5 et 6 de la fonction suivante, écrite en langage Python, afin qu'elle renvoie le plus petit rang à partir duquel les termes de la suite $\left(g_n\right)$ sont tous inférieurs ou égaux à $0,4 + e$, où $e$ est un nombre réel strictement positif.

%\begin{center}
%\begin{tabular}{l l}
%1{\tiny $\blacktriangledown$}& def seuil(e) :\\
%2 &g=0.5\\
%3 &n=1\\
%4{\tiny $\blacktriangledown$}&while \ldots  :\\
%5 &\qquad g = 0.5*g+0.2\\
%6 &\qquad n = \:\dots\\
%7 &return (n)\\
%\end{tabular}
%\end{center}

\begin{center}
\fbox{
\sffamily
\begin{tabular}{l}
1 \quad \textbf{def} seuil(e) :\\
2 \quad \qquad g = 0.5\\
3 \quad \qquad n = 1\\
4 \quad \qquad \textbf{while} \ldots  :\\
5 \quad \qquad \qquad g = 0.5 * g + 0.2 \hspace*{1cm}\\
6 \quad \qquad \qquad n = \dots\\
7 \quad \qquad \textbf{return} (n)\\
\end{tabular}
}
\end{center}
\end{enumerate}

\bigskip

\textbf{\textsc{Exercice 3} \hfill 4 points}

\medskip

\emph{Pour chacune des affirmations suivantes, indiquer si elle est vraie ou fausse.
Chaque réponse doit être justifiée. Une réponse non justifiée ne rapporte aucun point.}

\medskip

\begin{enumerate}
\item Soit $\left(u_n\right)$ une suite définie pour tout entier naturel $n$ et vérifiant la relation suivante:

\begin{center}pour tout entier naturel $n,\:\: \dfrac12  < u_n \leqslant \dfrac{3n^2 + 4n + 7}{6n^2 + 1}$.\end{center}

\textbf{Affirmation 1} $\displaystyle\lim_{n \to + \infty} u_n =\dfrac12$.
\item Soit $h$ une fonction définie et dérivable sur l'intervalle [-4~;~4].

La représentation graphique $\mathcal{C}_{h'}$ de sa fonction dérivée $h'$ est donnée ci-dessous.

\begin{center}
\psset{xunit=0.8cm,yunit=0.4cm,arrowsize=2pt 3}
\begin{pspicture}(-4,-5)(4.5,10)
\psaxes[linewidth=1.25pt,Dy=2](0,0)(-4,-5)(4.5,10)
\psplot[plotpoints=2000,linewidth=1.25pt,linecolor=blue]{-4}{4}{x  4 add  x 3 add mul x 1 add  mul x 3 sub mul  3 x sub mul x 3.85 sub  mul 90 div}
\uput[r](-3.2,2){\blue $\mathcal{C}_{h'}$}
\end{pspicture}
\end{center}

\textbf{Affirmation 2 :} La fonction $h$ est convexe sur $[-1~;~3]$.
\item Le code d'un immeuble est composé de 4 chiffres (qui peuvent être identiques) suivis de deux lettres distinctes parmi A, B et C (exemple: 1232BA).

\textbf{Affirmation 3 :} Il existe 20 634 codes qui contiennent au moins un 0.`

\item On considère la fonction $f$ définie sur $]0~;~+\infty[$ par $f(x) = x \ln x$.

\textbf{Affirmation 4 :}  La fonction $f$ est une solution sur $]0~;~ +\infty[$ de l'équation différentielle

\[xy' - y = x.\]
\end{enumerate}

\bigskip

\textbf{\textsc{Exercice 4} \hfill 5 points}

\medskip

Dans un repère orthonormé \Oijk{} de l'espace, on considère le plan $(P)$ d'équation: 
\[(P) :\quad 2x + 2y - 3z + 1 = 0.\]

On considère les trois points A, B et C de coordonnées: 
\begin{center}A(1~;~0~;~1) , B$(2~;~- 1~;~1)$ et C$( - 4~;~- 6~;~5)$ .\end{center}

Le but de cet exercice est d'étudier le rapport des aires entre un triangle et son projeté orthogonal dans un plan.


\medskip

\textbf{Partie A}

\begin{enumerate}
\item Pour chacun des points A, B et C, vérifier s'il appartient au plan $(P)$.
\item Montrer que le point C$'(0~;~- 2~;~- 1)$ est le projeté orthogonal du point C sur le plan $(P)$.
\item Déterminer une représentation paramétrique de la droite (AB).
\item On admet l'existence d'un unique point H vérifiant les deux conditions 

$\left\{\begin{array}{l}
\text{H} \in \text{(AB)}\\
\text{(AB) et (HC) sont orthogonales}.
\end{array}\right.$

Déterminer les coordonnées du point H.

\begin{center}
\psset{unit=1cm}
\begin{pspicture}(7.8,3.3)
\pspolygon(0,0)(5.5,0)(7.8,3.2)(2.3,3.2)%parallélo
\pspolygon(2.3,0.6)(4.7,1.8)(2.6,2.8)%ABC
\uput[d](2.3,0.6){A} \uput[r](4.7,1.8){B} \uput[ur](2.6,2.8){C}
\psline[linestyle=dashed](2.3,0.6)(2.6,1.8)(2.6,2.8)%AC'C
\psline[linestyle=dashed](2.6,1.8)(4.7,1.8)%C'B
\uput[ur](2.6,1.8){C$'$}\rput(5.4,0.2){$(P)$}\end{pspicture}
\end{center}

\end{enumerate}

\medskip

\textbf{Partie B}

\medskip

On admet que les coordonnées du vecteur $\vect{\text{HC}}$ sont : $\vect{\text{HC}}\begin{pmatrix}- \frac{11}{2}\\- \frac{11}{2}\\4\end{pmatrix}$.

\medskip

\begin{enumerate}
\item Calculer la valeur exacte de $\left\|\vect{\text{HC}}\right\|$.
\item Soit $S$ l'aire du triangle ABC. Déterminer la valeur exacte de $S$.
\end{enumerate}

\medskip

\textbf{Partie C}

\medskip

On admet que HC$'= \sqrt{\dfrac{17}{2}}$.

\medskip

\begin{enumerate}
\item Soit $\alpha = \widehat{\text{CHC}'}$. Déterminer la valeur de $\cos(\alpha)$.
\item 
	\begin{enumerate}
		\item Montrer que les droites (C$'$H) et (AB) sont perpendiculaires.
		\item Calculer $S'$ l'aire du triangle ABC$'$, on donnera la valeur exacte.
		\item Donner une relation entre $S,\: S'$ et $\cos (\alpha)$.
	\end{enumerate}
\end{enumerate}
\end{document}