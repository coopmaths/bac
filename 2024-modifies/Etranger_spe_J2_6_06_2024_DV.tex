\documentclass[11pt,a4paper,french]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{makeidx}
\usepackage{amsmath,amssymb}
\usepackage{fancybox}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{lscape}
\usepackage{multicol}
\usepackage{mathrsfs}
\usepackage{tabularx}
\usepackage{multirow}
\usepackage{enumitem}
\usepackage{textcomp}
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
%Relecture : 
\usepackage{pst-plot,pst-tree,pstricks,pst-node,pst-text}
\usepackage{pst-eucl,pst-3dplot,pstricks-add}
\usepackage{tikz}
\usepackage{esvect}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=2cm, bottom=3cm]{geometry}
\headheight15 mm
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\newcommand{\e}{\text{e}}
\usepackage{fancyhdr}
\usepackage[dvips]{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {Baccalauréat Spécialité},
pdftitle = {Centres étrangers (Europe) Sujet 2 6 juin 2024},
allbordercolors = white,
pdfstartview=FitH}
\usepackage{babel}
\usepackage[np]{numprint}
\renewcommand\arraystretch{1.3}
\frenchsetup{StandardLists=true}
\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Baccalauréat spécialité sujet 2}
\lfoot{\small{Centres étrangers}}
\rfoot{\small{6 juin 2024}}
\pagestyle{fancy}
\thispagestyle{empty}

\begin{center}{\Large\textbf{\decofourleft~Baccalauréat Centres étrangers\footnote{Europe} 6 juin 2024~\decofourright\\[7pt]  Sujet 2\\[7pt] ÉPREUVE D'ENSEIGNEMENT DE SPÉCIALITÉ}}
\end{center}

\medskip

\textbf{\textsc{Exercice 1} \hfill 5 points}

\medskip

Un sac opaque contient huit jetons numérotés de 1 à 8, indiscernables au toucher.

À trois reprises, un joueur pioche un jeton dans ce sac, note son numéro, puis le remet dans le sac.

Dans ce contexte, on appelle \og tirage \fg{} la liste ordonnée des trois numéros obtenus.

Par exemple, si le joueur pioche le jeton numéro 4, puis le jeton numéro 5, puis le jeton numéro 1, alors le tirage correspondant est (4~;~5~;~1).

\medskip

\begin{enumerate}
\item Déterminer le nombre de tirages possibles.
\item 
	\begin{enumerate}
		\item Déterminer le nombre de tirages sans répétition de numéro.
		\item En déduire le nombre de tirages contenant au moins une répétition de numéro.
	\end{enumerate}
\end{enumerate}
	
On note $X_1$ la variable aléatoire égale au numéro du premier jeton pioché, $X_2$ celle égale au numéro du deuxième jeton pioché et $X_3$ celle égale au numéro du troisième jeton pioché.

Puisqu'il s'agit d'un tirage avec remise, les variables aléatoires $X_1, X_2$, et $X_3$ sont indépendantes et suivent la même loi de probabilité.

\begin{enumerate}[resume]
\item Établir la loi de probabilité de la variable aléatoire $X_1$
\item Déterminer l'espérance de la variable aléatoire $X_1$
\end{enumerate}

On note $S = X_1 + X_2 + X_3$ la variable aléatoire égale à la somme des numéros des trois jetons piochés.

\begin{enumerate}[resume]
\item Déterminer l'espérance de la variable aléatoire $S$.
\item Déterminer $P(S = 24)$.
\item Si un joueur obtient une somme supérieure ou égale à $22$, alors il gagne un lot.
	\begin{enumerate}
		\item Justifier qu'il existe exactement $10$ tirages permettant de gagner un lot.
		\item En déduire la probabilité de gagner un lot.
	\end{enumerate}
\end{enumerate}

\medskip

\textbf{\textsc{Exercice 2} \hfill 6 points}

\medskip

On considère la fonction $f$ définie sur l'intervalle $]-\infty~;~1[$ par 

\[f(x) = \dfrac{\e^x}{x - 1}.\]

On admet que la fonction $f$ est dérivable sur l'intervalle $]-\infty~;~1[$ .

On appelle $\mathcal{C}$ sa courbe représentative dans un repère.

\medskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item Déterminer la limite de la fonction $f$ en 1.
		\item En déduire une interprétation graphique.
	\end{enumerate}
\item Déterminer la limite de la fonction $f$ en $-\infty$.
\item 
	\begin{enumerate}
		\item Montrer que pour tout réel $x$ de l'intervalle $]-\infty~;~1[$ , on a 
		\[f'(x) = \dfrac{(x - 2)\e^x}{(x - 1)^2}.\]
		
		\item Dresser, en justifiant, le tableau de variations de la fonction $f$ sur l'intervalle $]-\infty~;~1[$.

	\end{enumerate}
\item On admet que pour tout réel $x$ de l'intervalle ]$]-\infty~;~1[$, on a 

\[f''(x) = \dfrac{\left(x^2 -4x + 5\right)\e^x}{(x - 1)^3}.\]

	\begin{enumerate}
		\item Étudier la convexité de la fonction $f$ sur l'intervalle $]-\infty~;~1[$.
		\item Déterminer l'équation réduite de la tangente $T$ à la courbe $\mathcal{C}$ au point d'abscisse 0.
		\item En déduire que, pour tout réel $x$ de l'intervalle $]-\infty~;~1[$, on a :
		
\[\e^x \geqslant  (-2x - 1)(x-1).\]

	\end{enumerate}
\item 
	\begin{enumerate}
		\item Justifier que l'équation $f(x) = -2$ admet une unique solution $\alpha$ sur l'intervalle $]-\infty~;~1[$.
		\item À l'aide de la calculatrice, déterminer un encadrement de $\alpha$ d'amplitude $10^{-2}$.
	\end{enumerate}
\end{enumerate}

\medskip

\textbf{\textsc{Exercice 3} \hfill 5 points}

\medskip

Le cube ABCDEFGH a pour arête 1 cm.

Le point I est le milieu du segment [AB] et le point J est le milieu du segment [CG].

\begin{center}
\psset{unit=1cm}
\begin{pspicture}(6,7)
\psframe(0.3,0.3)(4.3,4.3)%ABFE
\psline(4.3,0.3)(5.3,2.3)(5.3,6.3)(4.3,4.3)%BCGF
\psline(5.3,6.3)(1.3,6.3)(0.3,4.3)%GHE
\psline[linestyle=dashed](0.3,0.3)(1.3,2.3)(5.3,2.3)%ADC
\psline[linestyle=dashed](1.3,2.3)(1.3,6.3)%DH
\uput[dl](0.3,0.3){A} \uput[dr](4.3,0.3){B} \uput[r](5.3,2.3){C} \uput[dr](1.3,2.3){D}
\uput[l](0.3,4.3){E} \uput[ul](4.3,4.3){F} \uput[ur](5.3,6.3){G} \uput[u](1.3,6.3){H}
\psdots[dotstyle=+,dotangle=45,dotscale=1.7](2.3,0.3)(5.3,4.3)
\uput[d](2.3,0.3){I}\uput[r](5.3,4.3){J}
\end{pspicture}
\end{center}

On se place dans le repère orthonormé $\left(\text{A}~;~\vect{\text{AB}},~\vect{\text{AD}},~~\vect{\text{AE}}\right)$.

\medskip

\begin{enumerate}
\item Donner les coordonnées des points I et J.\item Montrer que le vecteur $\vect{\text{EJ}}$ est normal au plan (FHI).
\item Montrer qu'une équation cartésienne du plan (FHI) est $- 2x - 2y + z + 1 = 0$.
\item Déterminer une représentation paramétrique de la droite (EJ).
\item 
	\begin{enumerate}
		\item On note K le projeté orthogonal du point E sur le plan (FHI). Calculer ses coordonnées.
		\item  Montrer que le volume de la pyramide EFHI est $\dfrac16$~cm$^3$.
		
\emph{On pourra utiliser le point {\rm L}, milieu du segment {\rm [EF]}. On admet que ce point est le projeté orthogonal du point {\rm I} sur le plan} (EFH).
		\item Déduire des deux questions précédentes l'aire du triangle FHI.
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{\textsc{Exercice 4} \hfill 4 points}

\medskip

\textbf{Partie A}

\medskip

On considère la fonction $f$ définie sur l'intervalle $[0~;~+\infty[$ par 
\[f(x) = \sqrt{x + 1}.\]

On admet que cette fonction est dérivable sur ce même intervalle.

\medskip

\begin{enumerate}
\item Démontrer que la fonction $f$ est croissante sur l'intervalle $[0~;~+\infty[$.
\item Démontrer que pour tout nombre réel $x$ appartenant à l'intervalle $[0~;~+\infty[$ :

\[f(x) - x = \dfrac{-x^2 + x + 1}{\sqrt{x + 1} + x}.\]

\item En déduire que sur l'intervalle $[0~;~+\infty[$ l'équation $f(x) = x$ admet pour unique solution :

\[\ell = \dfrac{1 +\sqrt 5}{2}.\]

\end{enumerate}

\medskip

\textbf{Partie B}

\medskip

On considère la suite $\left(u_n\right)$ définie par $u_0 = 5$ et pour tout entier naturel $n$, par 

$u_{n+1} = f\left(u_n\right)$ où $f$ est la fonction étudiée dans la \textbf{partie A}.

On admet que la suite de terme général $u_n$ est bien définie pour tout entier naturel $n$.

\medskip

\begin{enumerate}
\item Démontrer par récurrence que pour tout entier naturel $n$, on a 

\[1 \leqslant u_{n+1} \leqslant u_n.\]

\item En déduire que la suite $\left(u_n\right)$ converge.
\item Démontrer que la suite $\left(u_n\right)$ converge vers $\ell = \dfrac{1 +\sqrt 5}{2}$.
\item On considère le script Python ci-dessous :
\begin{center}
\begin{tabular}{|>{\ttfamily}l >{\ttfamily}l|}\hline
1 &\textbf{from} math \textbf{import} *\\
2 &\textbf{def} seuil(n):\\
3 &\qquad u=5\\
4 &\qquad i=$0$\\
5 &\qquad $\ell$ =(1 + \textbf{sqrt}(5))/2\\
6 &\qquad \textbf{while abs}(u-$\ell$)>=10**(-n):\\
7 &\qquad \qquad u=\textbf{sqrt}(u+1)\\
8 &\qquad \qquad i=i+1\\
9 &\qquad \textbf{return}(i)\\ \hline
\end{tabular}
\end{center}

\emph{On rappelle que la commande} \textbf{abs(x)} \emph{renvoie la valeur absolue de $x$}.

	\begin{enumerate}
		\item Donner la valeur renvoyée par {\ttfamily seuil} (2).
		\item La valeur renvoyée par {\ttfamily seuil} (4) est 9.
		
Interpréter cette valeur dans le contexte de l'exercice.
	\end{enumerate}
\end{enumerate}
\end{document}